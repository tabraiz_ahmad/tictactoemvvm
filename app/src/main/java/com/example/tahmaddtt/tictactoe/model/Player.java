package com.example.tahmaddtt.tictactoe.model;

/**
 * Created by tahmad.dtt on 8/16/18.
 */

public class Player {
    public String name;
    public String value;


    public Player(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
