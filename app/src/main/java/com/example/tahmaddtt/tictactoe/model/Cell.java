package com.example.tahmaddtt.tictactoe.model;

import com.example.tahmaddtt.tictactoe.Utils.StringUtils;

/**
 * Created by tahmad.dtt on 8/16/18.
 */

public class Cell {
    public Player player;


    public Cell(Player player) {
        this.player = player;
    }

    public boolean isEmpty() {
        return player == null || StringUtils.isNullOrEmpty(player.value);
    }
}
