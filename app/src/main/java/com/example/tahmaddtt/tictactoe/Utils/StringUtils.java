package com.example.tahmaddtt.tictactoe.Utils;

/**
 * Created by tahmad.dtt on 8/16/18.
 */

public class StringUtils {

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.length() < 1;
    }

    public static String stringFromNumbers(int... numbers) {
        StringBuilder sNumbers = new StringBuilder();
        for (int number : numbers)
            sNumbers.append(number);
        return sNumbers.toString();
    }
}
